<?php

class GetText
{
    private $connection;
    private $host;
    private $user;
    private $pass;
    private $db;

    public function __construct()
    {
        $this->host = 'localhost';
        $this->user = 'root';
        $this->pass = 'root';
        $this->db = 'wiki';

        $this->connection = new mysqli($this->host, $this->user, $this->pass, $this->db) or die("Could not connect to the database" .$this->connection->connect_error);
    }

    /**
     * @throws Exception
     */
    public function getText( $langCode, $id = null)
    {
        try {
            if ($id) {
                $result = $this->connection->query(
                    "SELECT p.*, t.text, t.title FROM pages as p LEFT JOIN translates as t ON p.id = t.entity_id LEFT JOIN languages as l ON l.id = t.lang_id WHERE p.id= " . $id . " AND l.code = '" . $langCode . "'"
                ) or die($this->connection->error);
            } else {
                $result = $this->connection->query(
                    "SELECT p.*, t.text, t.title FROM pages as p LEFT JOIN translates as t ON p.id = t.entity_id LEFT JOIN languages as l ON l.id = t.lang_id WHERE l.code = '" . $langCode . "'"
                ) or die($this->connection->error);
            }

            return $result->fetch_all(MYSQLI_ASSOC);
        } catch (Exception $e) {
            throw $e;
        }
    }


}