<?php
error_reporting(0);
require_once __DIR__ . '/vendor/autoload.php';

$googleAccountKeyFilePath = __DIR__ . '/sheet-329813-c779ffebf4b3.json';
putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath);

$client = new Google_Client();
$client->useApplicationDefaultCredentials();

$client->addScope('https://www.googleapis.com/auth/spreadsheets');

$dateFormat = "d.m.Y";

$service = new Google_Service_Sheets($client);

$spreadsheetId = '1OfqCM5z-ym3sFhJADkmjrbbeu6htphwU_FM33gTAopU';

$response = $service->spreadsheets->get($spreadsheetId);

$spreadsheetProperties = $response->getProperties();

$rowCount = 1;

foreach ($response->getSheets() as $sheet) {
    $sheetProperties = $sheet->getProperties();
    $gridProperties = $sheetProperties->getGridProperties();
    $rowCount = $gridProperties->rowCount;
}

$range = 'Лист1!A1:A' . $rowCount;
$response = $service->spreadsheets_values->get($spreadsheetId, $range);

$searchValue = date($dateFormat);

$numRow = false;
$founded = false;
foreach ($response->values as $i => $rowData) {
    $numRow = $i + 1;

    if (isset($rowData[0]) && $rowData[0] == $searchValue) {
        $founded = true;
        break;
    }
}
$randomInt = random_int(1, 1000000);

if ($founded) {
    $body = new Google_Service_Sheets_ValueRange(
        [
            'values' => [
                [$randomInt],
            ]
        ]
    );
    $options = array('valueInputOption' => 'RAW');

    $service->spreadsheets_values->update($spreadsheetId, 'Лист1!B' . $numRow, $body, $options);
} else {
    $body = new Google_Service_Sheets_ValueRange(
        [
            'values' => [
                [$searchValue, $randomInt],
            ]
        ]
    );
    $options = array('valueInputOption' => 'RAW');

    $service->spreadsheets_values->append($spreadsheetId, 'Лист1', $body, $options);
}
echo 'Row is updated or inserted';