<?php

$controllerName = 'Main';
$actionName = 'index';
$uri = $_SERVER['REQUEST_URI'];
$uri = str_replace('2_mvc/', '', $uri); //optional for remove subdirectory

$uri = rawurldecode($uri);

$routes = explode('/', $uri);

if (!empty($routes[1])) {
    if (strpos($routes[1], '_') !== false) {
        $actionParts = explode('_', $routes[1]);
        $controllerName = $actionParts[1];
        $actionName = $actionParts[0];
    } else {
        $controllerName = $routes[1] ?? $controllerName;
        $actionName = $routes[2] ?? $actionName;
    }
    $params = $routes[3] ?? '';
}

$controllerName = ucfirst($controllerName) . 'Controller';
$actionName = ucfirst($actionName) . 'Action';

$controllerFile = $controllerName . '.php';
$controllerPath = "application/controllers/" . $controllerFile;
if (file_exists($controllerPath)) {
    include "application/controllers/" . $controllerFile;
} else {
    ErrorPage404();
}

$controller = new $controllerName;
$action = $actionName;

if (method_exists($controller, $action)) {
    $controller->$action($params);
} else {
    ErrorPage404();
}


function ErrorPage404()
{
    $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
    header('HTTP/1.1 404 Not Found');
    header("Status: 404 Not Found");
    header('Location:' . $host . '2_mvc/error404');
}
